<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property int|null $idCocheAlquilado
 * @property string|null $fechaAlquiler
 *
 * @property Coches $idCocheAlquilado0
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['cod', 'idCocheAlquilado'], 'integer'],
            [['fechaAlquiler'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['idCocheAlquilado'], 'unique'],
            [['cod'], 'unique'],
            [['idCocheAlquilado'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['idCocheAlquilado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'idCocheAlquilado' => 'Id Coche Alquilado',
            'fechaAlquiler' => 'Fecha Alquiler',
        ];
    }

    /**
     * Gets query for [[IdCocheAlquilado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCocheAlquilado0()
    {
        return $this->hasOne(Coches::className(), ['id' => 'idCocheAlquilado']);
    }
}
